README.txt
==========

Subscription Manager (subman) is a modular Java based application for managing subscription based memberships. It would
be suitable for managing the memberships for a club, association or society (known as a 'Community' in subman).

The application allows the administrator of a Community to add and remove members, edit the data for members, and track
payment information. Users can be eneterd into the systemmanually, or imported from a CSV file.

* [Unit test report](https://bug_or_feature.gitlab.io/subman/test/index.html)
* [Coverage report](https://bug_or_feature.gitlab.io/subman/jacoco/index.html)

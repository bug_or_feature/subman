/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package submanx;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import net.bugorfeature.subman.SubmanApplication;

/**
 * Config for integration tests
 *
 * NOTE: the package 'submanx' exists so that the classes it contains will not be picked up by component scanning.
 *
 * @author Andy Geach
 */
@Configuration
@Import(SubmanApplication.class)
public class ITCaseConfig {

}

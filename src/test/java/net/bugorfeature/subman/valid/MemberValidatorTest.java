package net.bugorfeature.subman.valid;

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.junit.Before;
import org.junit.Test;
import org.springframework.validation.BindException;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.model.Member;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Unit test for MemberValidator
 *
 * @author Andy Geach
 */
public class MemberValidatorTest {

    private Validator validator;
    private MemberBuilder builder;

    @Before
    public void setUp() throws Exception {
        validator = new MemberValidator();
        builder = new MemberBuilder();
    }

    @Test
    public void supports() {
        assertTrue(validator.supports(Member.class));
        assertFalse(validator.supports(Object.class));
    }

    @Test
    public void validate() {
        Member target = builder.build();
        BindException errors = new BindException(target, "member");
        ValidationUtils.invokeValidator(validator, target, errors);
        assertFalse(errors.hasErrors());
    }
}

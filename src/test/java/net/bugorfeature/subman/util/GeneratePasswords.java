/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.util.Assert;

import net.bugorfeature.subman.config.PasswordEncoderConfig;

/**
 * Generate encoded passwords for use in data setup sql scripts
 *
 * @author Andy Geach
 */
public class GeneratePasswords {

    public static void main(String[] args) {

        try {
            Assert.isTrue(args.length > 0);

            ApplicationContext context = new AnnotationConfigApplicationContext(PasswordEncoderConfig.class);
            PasswordEncoder passwordEncoder = context.getBean(PasswordEncoder.class);
            for (String pw : args) {

                String[] param = pw.split(":");
                StringBuilder builder = new StringBuilder();
                builder.append("('").append(param[0]).append("', '").append(passwordEncoder.encode(param[1])).append("', TRUE),");
                System.out.println(builder.toString());
            }
        } catch (Exception e) {
            usage();
        }
    }

    private static void usage() {
        System.out.println("Usage: java net.bugorfeature.subman.util.GeneratePasswords username:password [username:password]...");
    }
}

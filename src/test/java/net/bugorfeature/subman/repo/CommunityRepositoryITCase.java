/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.repo;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.User;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Community repo integration test
 *
 * @author Andy Geach
 */
public class CommunityRepositoryITCase extends AbstractRepositoryITCase {

    private static final Logger log = LoggerFactory.getLogger(CommunityRepositoryITCase.class);

    @Autowired
    CommunityRepository repository;

    @Autowired
    UserRepository userRepository;

    @Test
    public void testInsert() {

        String name = "Garden Club";
        String about = "Gardens";

        Community community = new CommunityBuilder().build();
        community.setName(name);
        community.setAbout(about);

        community = repository.save(community);

        Community retrievedCommunity = repository.findOne(community.getIdNumber());

        assertNotNull(retrievedCommunity);
        assertEquals(name, retrievedCommunity.getName());
        assertEquals(about, retrievedCommunity.getAbout());
    }


    @Test
    public void testUsers() {

        String name = "Garden Club";
        String about = "Gardens";

        Community community = new CommunityBuilder().build();
        community.setName(name);
        community.setAbout(about);
        community = repository.save(community);

        User user = new User();
        user.setUsername("jimmy");
        user.setPassword("password");
        user.setEnabled(true);
        user = userRepository.save(user);

        Set<User> users = new HashSet<>();
        users.add(user);
        community.setUsers(users);
        community = repository.save(community);

        Set<Community> communities = new HashSet<>();
        communities.add(community);
        user.setCommunities(communities);
        user = userRepository.save(user);

        List<Community> communityList = repository.findByUsersUsername("jimmy");
        assertTrue(communityList.size() == 1);
    }
}

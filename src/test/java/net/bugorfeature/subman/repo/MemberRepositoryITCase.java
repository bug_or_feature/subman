/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.repo;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.model.Member;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;

/**
 * Member repo integration test
 *
 * @author Andy Geach
 */
public class MemberRepositoryITCase extends AbstractRepositoryITCase {

    private static final Logger log = LoggerFactory.getLogger(MemberRepositoryITCase.class);

    @Autowired
    MemberRepository repository;

    @Test
    public void testInsert() {

        String telephoneNo = "01284 123123";

        Member member = MemberBuilder.aMember().build();
        member.setTelephoneNo(telephoneNo);
        member = repository.save(member);

        log.info("first: " + member);

        Member retrievedMember = repository.findByIdNumber(member.getIdNumber());

        log.info("second: " + retrievedMember);

        assertNotNull(retrievedMember);
        assertEquals("Jimmy", retrievedMember.getFirstName());
        assertEquals("Jazz", retrievedMember.getLastName());
        assertEquals("Abbeygate", retrievedMember.getCustomField_1());
    }
}

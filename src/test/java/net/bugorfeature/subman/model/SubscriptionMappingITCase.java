/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import org.junit.Test;

import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableExists;
import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableHasColumn;

/**
 * Integration test for Subscription JPA mappings
 *
 * @author Andy Geach
 */
public class SubscriptionMappingITCase extends AbstractMappingITCase {

    @Test
    public void testMapping() {
        assertTableExists(manager, "SUBSCRIPTION");
        assertTableHasColumn(manager, "SUBSCRIPTION", "SUBSCRIPTION_ID");
        assertTableHasColumn(manager, "SUBSCRIPTION", "COMMUNITY_ID");
        assertTableHasColumn(manager, "SUBSCRIPTION", "MEMBER_ID");
        assertTableHasColumn(manager, "SUBSCRIPTION", "SUB_NUMBER");
        assertTableHasColumn(manager, "SUBSCRIPTION", "CUSTOM_FIELD_1");
        assertTableHasColumn(manager, "SUBSCRIPTION", "DUE_AMOUNT");
        assertTableHasColumn(manager, "SUBSCRIPTION", "DUE_DATE");
        assertTableHasColumn(manager, "SUBSCRIPTION", "PAID_AMOUNT");
        assertTableHasColumn(manager, "SUBSCRIPTION", "PAID_DATE");
        assertTableHasColumn(manager, "SUBSCRIPTION", "PAYMENT_METHOD");
        assertTableHasColumn(manager, "SUBSCRIPTION", "FREQUENCY");
        assertTableHasColumn(manager, "SUBSCRIPTION", "STATUS");
        assertTableHasColumn(manager, "SUBSCRIPTION", "CARD_ISSUED");
    }
}

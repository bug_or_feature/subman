/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import org.junit.Ignore;
import org.junit.Test;

import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableExists;
import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableHasColumn;

/**
 * Integration test for the Security tables and mappings
 *
 * @author Andy Geach
 */
public class SecurityMappingITCase extends AbstractMappingITCase {

    @Test
    public void testUser() {
        assertTableExists(manager, "user_table");
        assertTableHasColumn(manager, "user_table", "USERNAME");
        assertTableHasColumn(manager, "user_table", "PASSWORD");
        assertTableHasColumn(manager, "user_table", "ENABLED");
    }

    @Ignore  // TODO how to test with embedded db? these entities are not mapped
    @Test
    public void testAuthorities() {
        assertTableExists(manager, "authority");
        assertTableHasColumn(manager, "authority", "USERNAME");
        assertTableHasColumn(manager, "authority", "AUTHORITY");
    }

    @Ignore  // TODO how to test with embedded db? these entities are not mapped
    @Test
    public void testPersistentLogins() {
        assertTableExists(manager, "persistent_logins");
        assertTableHasColumn(manager, "persistent_logins", "SERIES");
        assertTableHasColumn(manager, "persistent_logins", "USERNAME");
        assertTableHasColumn(manager, "persistent_logins", "TOKEN");
        assertTableHasColumn(manager, "persistent_logins", "LAST_USED");
    }
}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import org.junit.Test;

import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableExists;
import static net.bugorfeature.subman.model.fixture.JPAAssertions.assertTableHasColumn;

/**
 * Integration test for JPA mappings
 *
 * @author Andy Geach
 */
public class MemberMappingITCase extends AbstractMappingITCase {

    @Test
    public void testMapping() {
        assertTableExists(manager, "MEMBER");
        assertTableHasColumn(manager, "MEMBER", "MEMBER_ID");
        assertTableHasColumn(manager, "MEMBER", "FIRST_NAME");
        assertTableHasColumn(manager, "MEMBER", "INITIALS");
        assertTableHasColumn(manager, "MEMBER", "LAST_NAME");
        assertTableHasColumn(manager, "MEMBER", "GENDER");
        assertTableHasColumn(manager, "MEMBER", "SALUTATION");
        assertTableHasColumn(manager, "MEMBER", "ADDRESS_1");
        assertTableHasColumn(manager, "MEMBER", "ADDRESS_2");
        assertTableHasColumn(manager, "MEMBER", "ADDRESS_3");
        assertTableHasColumn(manager, "MEMBER", "TOWN_CITY");
        assertTableHasColumn(manager, "MEMBER", "COUNTY");
        assertTableHasColumn(manager, "MEMBER", "POSTCODE");
        assertTableHasColumn(manager, "MEMBER", "EMAIL_ADDRESS");
        assertTableHasColumn(manager, "MEMBER", "TELEPHONE_NO");
        assertTableHasColumn(manager, "MEMBER", "COMM_PREFS");
        assertTableHasColumn(manager, "MEMBER", "CUSTOM_FIELD_1");
    }
}

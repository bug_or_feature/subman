/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service;

import java.io.IOException;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import submanx.ITCaseConfig;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.config.CsvLoaderConfig;
import net.bugorfeature.subman.config.ValidationConfig;
import net.bugorfeature.subman.loader.ImportResult;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;

import static org.junit.Assert.assertTrue;

/**
 * Integration test for the Status Rule service
 *
 * @author Andy Geach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
@SpringBootTest(classes = {
        ITCaseConfig.class,
        CsvLoaderConfig.class,
        ValidationConfig.class
})
@WebAppConfiguration
@ActiveProfiles("itcase")
public class ImportServiceITCase {

    private static final Logger LOG = LoggerFactory.getLogger(ImportServiceITCase.class);

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    CommunityRepository communityRepository;

    @Autowired
    ImportService service;

    @Value("classpath:data/hhcc_example.csv")
    private Resource goodFile;

    @Value("classpath:data/hhcc_bad_member.csv")
    private Resource badFile;

    Subscription subOne;
    Community community;

    @Test
    public void test() throws IOException {

        assertTrue(communityRepository.findOne(community.getIdNumber()).getSubscriptions().size() == 1);

        ImportResult result = service.doImport(community, goodFile.getInputStream());

        assertTrue(result.getSuccessCount() == 9);
        assertTrue(result.getIssues().size() == 0);
    }

    @Test
    public void problems() throws IOException {

        assertTrue(communityRepository.findOne(community.getIdNumber()).getSubscriptions().size() == 1);

        ImportResult result = service.doImport(community, badFile.getInputStream());

        assertTrue(result.getSuccessCount() == 0);
        assertTrue(result.getIssues().size() == 1);
    }

    @Before
    public void setup() {

        community = CommunityBuilder.aCommunity().build();
        communityRepository.save(community);

        Member memberOne = MemberBuilder.aMember().build();
        memberRepository.save(memberOne);

        Subscription subOne = SubscriptionBuilder.aSubscription().community(community).member(memberOne).number("1").dueDate(new LocalDate().minusDays(1)).build();
        community.addSubscription(subOne);

        subscriptionRepository.save(subOne);
    }
}

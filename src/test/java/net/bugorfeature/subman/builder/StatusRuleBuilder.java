/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.builder;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.StatusRule;
import net.bugorfeature.subman.model.SubscriptionStatus;

/**
 * Builder class for Subscription instances
 *
 * @author Andy Geach
 */
public class StatusRuleBuilder {

    private Community community;
    private SubscriptionStatus status = SubscriptionStatus.EXPIRED;
    private Integer dayCount = 3;

    public static StatusRuleBuilder aStatusRule() {
        return new StatusRuleBuilder();
    }

    public StatusRuleBuilder community(Community community) {
        this.community = community;
        return this;
    }

    public StatusRuleBuilder status(SubscriptionStatus status) {
        this.status = status;
        return this;
    }

    public StatusRuleBuilder dayCount(Integer dayCount) {
        this.dayCount = dayCount;
        return this;
    }

    public StatusRule build() {

        StatusRule instance = new StatusRule();
        instance.setCommunity(community);
        instance.setStatus(status);
        instance.setDayCount(dayCount);
        community.addStatusRule(instance);

        return instance;
    }
}

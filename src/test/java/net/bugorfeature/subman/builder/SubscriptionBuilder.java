/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.builder;

import java.math.BigDecimal;

import org.joda.time.LocalDate;

import net.bugorfeature.subman.model.Branch;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.SubscriptionType;

/**
 * Builder class for Subscription instances
 *
 * @author Andy Geach
 */
public class SubscriptionBuilder {

    private Member member;
    private Community community;
    private BigDecimal dueAmount = new BigDecimal("25.00");
    private LocalDate dueDate = new LocalDate().minusDays(5);
    private LocalDate paidDate = new LocalDate().minusDays(5);
    private BigDecimal paidAmount = new BigDecimal("25.00");
    private SubscriptionFrequency frequency = SubscriptionFrequency.ANNUAL;
    private String customField_1 = Branch.BURY_TOWN.name();
    private PaymentMethod paymentMethod = PaymentMethod.STANDING_ORDER;
    private Boolean cardIssued = Boolean.TRUE;
    private SubscriptionStatus status = SubscriptionStatus.CURRENT;
    private String number;
    private SubscriptionType subscriptionType = SubscriptionType.MEMBER;
    private LocalDate memberSince = new LocalDate().minusMonths(2);

    public static SubscriptionBuilder aSubscription() {
        return new SubscriptionBuilder();
    }

    public SubscriptionBuilder member(Member member) {
        this.member = member;
        return this;
    }

    public SubscriptionBuilder community(Community community) {
        this.community = community;
        return this;
    }

    public SubscriptionBuilder number(String number) {
        this.number = number;
        return this;
    }

    public SubscriptionBuilder memberType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
        return this;
    }

    public SubscriptionBuilder dueAmount(BigDecimal dueAmount) {
        this.dueAmount = dueAmount;
        return this;
    }

    public SubscriptionBuilder dueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
        return this;
    }

    public SubscriptionBuilder paidDate(LocalDate paidDate) {
        this.paidDate = paidDate;
        return this;
    }

    public SubscriptionBuilder paidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
        return this;
    }

    public SubscriptionBuilder frequency(SubscriptionFrequency frequency) {
        this.frequency = frequency;
        return this;
    }

    public SubscriptionBuilder customField_1(String custom) {
        this.customField_1 = custom;
        return this;
    }

    public SubscriptionBuilder paymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
        return this;
    }

    public SubscriptionBuilder cardIssued(Boolean cardIssued) {
        this.cardIssued = cardIssued;
        return this;
    }

    public SubscriptionBuilder status(SubscriptionStatus status) {
        this.status = status;
        return this;
    }

    public SubscriptionBuilder memberSince(LocalDate memberSince) {
        this.memberSince = memberSince;
        return this;
    }

    public Subscription build() {

        Subscription instance = new Subscription();
        instance.setMember(member);
        instance.setCommunity(community);
        instance.setNumber(number);
        instance.setDueAmount(dueAmount);
        instance.setDueDate(dueDate);
        instance.setPaidDate(paidDate);
        instance.setPaidAmount(paidAmount);
        instance.setFrequency(frequency);
        instance.setCustomField_1(customField_1);
        instance.setPaymentMethod(paymentMethod);
        instance.setCardIssued(cardIssued);
        instance.setStatus(status);
        instance.setMemberSince(memberSince);

        return instance;
    }
}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.builder;

import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.SalutationType;

/**
 * Builder class for Member instances
 *
 * @author Andy Geach
 */
public class MemberBuilder {

    private SalutationType salutation = SalutationType.MR;
    private String firstName = "Jimmy";
    private String lastName = "Jazz";
    private String initials;
    private Gender gender = Gender.M;
    private String address1 = "1 South End";
    private String address2;
    private String address3;
    private String townCity = "Bristol";
    private String county = "Avon";
    private String postCode = "BS1 2AG";
    private String telephoneNo;
    private String emailAddress = "jimmy@traders.com";
    private String customField1 = "Abbeygate";
    private CommunicationPreferencesType commPrefs = CommunicationPreferencesType.PHONE;

    public static MemberBuilder aMember() {
        return new MemberBuilder();
    }

    public MemberBuilder salutation(SalutationType salutation) {
        this.salutation = salutation;
        return this;
    }

    public MemberBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public MemberBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public MemberBuilder initials(String initials) {
        this.initials = initials;
        return this;
    }

    public MemberBuilder gender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public MemberBuilder address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public MemberBuilder address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public MemberBuilder address3(String address3) {
        this.address3 = address3;
        return this;
    }

    public MemberBuilder townCity(String townCity) {
        this.townCity = townCity;
        return this;
    }

    public MemberBuilder county(String county) {
        this.county = county;
        return this;
    }

    public MemberBuilder postCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public MemberBuilder telephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
        return this;
    }

    public MemberBuilder emailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public MemberBuilder commPrefs(CommunicationPreferencesType commPrefs) {
        this.commPrefs = commPrefs;
        return this;
    }

    public MemberBuilder customField_1(String customField1) {
        this.customField1 = customField1;
        return this;
    }

    public Member build() {
        Member instance = new Member();

        instance.setFirstName(firstName);
        instance.setLastName(lastName);
        instance.setSalutation(salutation);
        instance.setInitials(initials);
        instance.setGender(gender);
        instance.setAddress1(address1);
        instance.setAddress2(address2);
        instance.setAddress3(address3);
        instance.setTownCity(townCity);
        instance.setCounty(county);
        instance.setPostCode(postCode);
        instance.setTelephoneNo(telephoneNo);
        instance.setEmailAddress(emailAddress);
        instance.setCommPrefs(commPrefs);
        instance.setCustomField_1(customField1);

        return instance;
    }
}

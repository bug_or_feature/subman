/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.builder;

import net.bugorfeature.subman.form.MemberForm;
import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.SalutationType;

/**
 * Member form builder
 *
 * @author Andy Geach
 */
public class MemberFormBuilder {

    private Integer idNumber;
    private SalutationType salutation = SalutationType.MISS;
    private String firstName = "Helen";
    private String lastName = "Harper";
    private String initials;
    private Gender gender = Gender.F;
    private String address1 = "3 The Vale";
    private String address2;
    private String address3;
    private String townCity = "Exeter";
    private String county = "Cornwall";
    private String postCode  = "EX1 1AA";
    private String telephoneNo;
    private String emailAddress = "helen@helenharper.me";
    private CommunicationPreferencesType commPrefs = CommunicationPreferencesType.NONE;
    private String customField_1;

    private MemberFormBuilder() {
    }

    public static MemberFormBuilder aMemberForm() {
        return new MemberFormBuilder();
    }

    public MemberFormBuilder withIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
        return this;
    }

    public MemberFormBuilder withSalutation(SalutationType salutation) {
        this.salutation = salutation;
        return this;
    }

    public MemberFormBuilder withFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public MemberFormBuilder withLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public MemberFormBuilder withInitials(String initials) {
        this.initials = initials;
        return this;
    }

    public MemberFormBuilder withGender(Gender gender) {
        this.gender = gender;
        return this;
    }

    public MemberFormBuilder withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public MemberFormBuilder withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public MemberFormBuilder withAddress3(String address3) {
        this.address3 = address3;
        return this;
    }

    public MemberFormBuilder withTownCity(String townCity) {
        this.townCity = townCity;
        return this;
    }

    public MemberFormBuilder withCounty(String county) {
        this.county = county;
        return this;
    }

    public MemberFormBuilder withPostCode(String postCode) {
        this.postCode = postCode;
        return this;
    }

    public MemberFormBuilder withTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
        return this;
    }

    public MemberFormBuilder withEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
        return this;
    }

    public MemberFormBuilder withCommPrefs(CommunicationPreferencesType commPrefs) {
        this.commPrefs = commPrefs;
        return this;
    }

    public MemberFormBuilder withCustomField_1(String customField_1) {
        this.customField_1 = customField_1;
        return this;
    }

    public MemberFormBuilder but() {
        return aMemberForm().withIdNumber(idNumber).withSalutation(salutation).withFirstName(firstName)
                .withLastName(lastName).withInitials(initials).withGender(gender).withAddress1(address1)
                .withAddress2(address2).withAddress3(address3).withTownCity(townCity).withCounty(county)
                .withPostCode(postCode).withTelephoneNo(telephoneNo).withEmailAddress(emailAddress)
                .withCommPrefs(commPrefs).withCustomField_1(customField_1);
    }

    public MemberForm build() {
        MemberForm memberForm = new MemberForm();
        memberForm.setIdNumber(idNumber);
        memberForm.setSalutation(salutation);
        memberForm.setFirstName(firstName);
        memberForm.setLastName(lastName);
        memberForm.setInitials(initials);
        memberForm.setGender(gender);
        memberForm.setAddress1(address1);
        memberForm.setAddress2(address2);
        memberForm.setAddress3(address3);
        memberForm.setTownCity(townCity);
        memberForm.setCounty(county);
        memberForm.setPostCode(postCode);
        memberForm.setTelephoneNo(telephoneNo);
        memberForm.setEmailAddress(emailAddress);
        memberForm.setCommPrefs(commPrefs);
        memberForm.setCustomField_1(customField_1);
        return memberForm;
    }
}

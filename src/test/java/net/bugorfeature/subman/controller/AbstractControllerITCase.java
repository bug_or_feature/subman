/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;

import submanx.ITCaseConfig;
import submanx.MockedServiceConfig;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;
import net.bugorfeature.subman.repo.CommunityRepository;
import net.bugorfeature.subman.repo.MemberRepository;
import net.bugorfeature.subman.repo.SubscriptionRepository;
import net.bugorfeature.subman.service.CommunityService;
import net.bugorfeature.subman.service.ImportService;
import net.bugorfeature.subman.service.MemberService;
import net.bugorfeature.subman.service.SubscriptionService;

import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

/**
 * Controller integration test super class
 *
 * @author Andy Geach
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {ITCaseConfig.class, MockedServiceConfig.class})
@WebAppConfiguration
@Transactional
@WithMockUser(roles = "USER")
public abstract class AbstractControllerITCase {

    protected MediaType contentType = new MediaType(MediaType.TEXT_HTML.getType(),
            MediaType.TEXT_HTML.getSubtype(),
            Charset.forName("utf8"));

    protected MediaType jsonContentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
                        Charset.forName("utf8"));

    protected MockMvc mockMvc;
    protected Community community;
    protected Member member;
    protected Integer communityId;
    protected Integer memberId;
    protected Subscription subscription;

    @Autowired
    protected CommunityRepository communityRepo;

    @Autowired
    protected SubscriptionRepository subscriptionRepo;

    @Autowired
    protected MemberRepository memberRepo;

    @Autowired
    protected WebApplicationContext webApplicationContext;

    @Autowired
    protected SubscriptionService subscriptionService;

    @Autowired
    protected MemberService memberService;

    @Autowired
    protected CommunityService communityService;

    @Autowired
    protected ImportService importService;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext)
                .apply(SecurityMockMvcConfigurers.springSecurity())
                .build();

        community = communityRepo.save(CommunityBuilder.aCommunity().build());
        communityId = community.getIdNumber();

        member = memberRepo.save(MemberBuilder.aMember().build());
        memberId = member.getIdNumber();

        subscription = SubscriptionBuilder.aSubscription().member(member).community(community).build();
        subscription = subscriptionRepo.save(subscription);
    }
}

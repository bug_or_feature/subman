/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration test for download controller
 *
 * @author Andy Geach
 */
public class DownloadControllerITCase extends AbstractControllerITCase {

    @Test
    public void xls() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        List<Subscription> list = new ArrayList<>();
        list.add(subscription);
        when(subscriptionService.getSubscribersForCommunity(any(Community.class))).thenReturn(list);
        mockMvc.perform(get("/doc/xls").sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("subs"))
                .andExpect(content().contentTypeCompatibleWith("application/vnd.ms-excel"))
                .andExpect(view().name("excelView"));
    }

    @Test
    public void pdf() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        List<Subscription> list = new ArrayList<>();
        list.add(subscription);
        when(subscriptionService.getSubscribersForCommunity(any(Community.class))).thenReturn(list);
        mockMvc.perform(get("/doc/pdf").sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("subs"))
                .andExpect(content().contentTypeCompatibleWith("application/pdf"))
                .andExpect(view().name("pdfView"));
    }
}

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import net.bugorfeature.subman.builder.MemberBuilder;
import net.bugorfeature.subman.builder.MemberFormBuilder;
import net.bugorfeature.subman.builder.SubscriptionBuilder;
import net.bugorfeature.subman.form.MemberForm;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.Subscription;

import static net.bugorfeature.subman.controller.SubscriptionController.FREQUENCY_TYPES_KEY;
import static net.bugorfeature.subman.controller.SubscriptionController.STATUS_TYPES_KEY;
import static org.hamcrest.core.StringStartsWith.startsWith;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration test
 *
 * @author Andy Geach
 */
public class SubscriptionControllerITCase extends AbstractControllerITCase {

    @Test
    public void newSubscription() throws Exception {
        mockMvc.perform(get("/subscription/new"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("paymentTypes", FREQUENCY_TYPES_KEY, STATUS_TYPES_KEY))
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("createOrEditSubscription"))
                .andExpect(xpath("//select[@id='customField_1']").doesNotExist());
    }

    @Test
    public void newBseccaSubscription() throws Exception {
        mockMvc.perform(get("/subscription/new").sessionAttr("communityName", "BSECCA"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("paymentTypes", FREQUENCY_TYPES_KEY, STATUS_TYPES_KEY))
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("createOrEditSubscription"))
                .andExpect(xpath("//select[@id='customField_1']").exists());
    }

    @Test
    public void setupEditInvalid() throws Exception {
        when(subscriptionService.getSubscription(anyInt())).thenReturn(null);
        mockMvc.perform(get("/subscription/edit/{memberId}", 1).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(flash().attributeExists("warning"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void setupEditValid() throws Exception {
        when(subscriptionService.getSubscription(anyInt(), anyInt())).thenReturn(subscription);
        mockMvc.perform(get("/subscription/edit/{memberId}", memberId).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("paymentTypes", FREQUENCY_TYPES_KEY, STATUS_TYPES_KEY))
                .andExpect(view().name("createOrEditSubscription"));
    }

    @Test
    public void setupPay() throws Exception {
        mockMvc.perform(get("/subscription/pay/{memberId}", memberId).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(model().attributeExists(FREQUENCY_TYPES_KEY, STATUS_TYPES_KEY, "subscription"))
                .andExpect(status().isOk())
                .andExpect(view().name("addPayment"));
    }

    @Test
    public void apiSubscribers() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        List<Subscription> list = new ArrayList<>();
        list.add(subscription);
        when(subscriptionService.getSubscribersForCommunity(any(Community.class))).thenReturn(list);
        mockMvc.perform(get("/api/subscriberList").sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(jsonContentType));
    }

    @Test
    public void apiSubscribersEmpty() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        Member member2 = memberRepo.save(MemberBuilder.aMember().firstName("first").lastName("last").build());
        Subscription subscription2 = SubscriptionBuilder.aSubscription().member(member2).community(community).number("1232123123").customField_1(null).paidDate(null).memberSince(null).build();
        subscription2 = subscriptionRepo.save(subscription2);
        List<Subscription> list = new ArrayList<>();
        list.add(subscription2);
        when(subscriptionService.getSubscribersForCommunity(any(Community.class))).thenReturn(list);
        mockMvc.perform(get("/api/subscriberList").sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(jsonContentType));
    }

    @Test
    public void submitInvalid() throws Exception {
        mockMvc.perform(post("/subscription/submit").with(csrf())
                .param("dueAmount", "bad"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("createOrEditSubscription"));
    }

    @Test
    public void submitUpdateValid() throws Exception {
        when(subscriptionService.getSubscription(anyInt())).thenReturn(subscription);
        mockMvc.perform(post("/subscription/submit").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).with(csrf())
                .param("idNumber", "1234")
                .param("dueAmount", "20.00")
                .param("dueDate", "2016-02-01")
                .param("paidAmount", "20.00")
                .param("paidDate", "2016-02-01")
                .param("subscriptionType", "MEMBER")
                .param("frequency", "ANNUAL")
                .param("status", "CURRENT")
                .param("paymentMethod", "CASH")
                .param("memberId", "1234")
                .param("memberSince", "2015-02-01")
                .param("originalDueDate", "2016-02-01"))
                .andDo(print())
                .andExpect(flash().attributeExists("message"))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void submitNewValid() throws Exception {
        when(subscriptionService.getSubscription(anyInt())).thenReturn(subscription);
        when(memberService.save(any(Member.class))).thenReturn(member);
        MemberForm memberForm = MemberFormBuilder.aMemberForm().build();
        mockMvc.perform(post("/subscription/submit").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).sessionAttr("newMember", memberForm).with(csrf())
                .param("dueAmount", "20.00")
                .param("dueDate", "2016-02-01")
                .param("paidAmount", "20.00")
                .param("paidDate", "2016-02-01")
                .param("subscriptionType", "MEMBER")
                .param("frequency", "ANNUAL")
                .param("status", "CURRENT")
                .param("paymentMethod", "CASH")
                .param("memberId", "1234")
                .param("memberSince", "2015-02-01")
                .param("originalDueDate", "2016-02-01"))
                .andDo(print())
                .andExpect(flash().attribute("message", startsWith("Subscription for member")))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void paymentInvalid() throws Exception {
        mockMvc.perform(post("/subscription/submitPayment").with(csrf())
                .param("dueAmount", "bad"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("addPayment"));
    }

    @Test
    public void paymentValid() throws Exception {
        when(subscriptionService.getSubscription(anyInt())).thenReturn(subscription);
        mockMvc.perform(post("/subscription/submitPayment").with(csrf())
                .param("memberId", subscription.getIdNumber().toString())
                .param("dueAmount", "20.00")
                .param("dueDate", "2016-02-01")
                .param("paidAmount", "20.00")
                .param("paidDate", "2016-02-01")
                .param("subscriptionType", "MEMBER")
                .param("frequency", "ANNUAL")
                .param("status", "CURRENT")
                .param("paymentMethod", "CASH")
                .param("memberId", "1234")
                .param("memberSince", "2015-02-01")
                .param("originalDueDate", "2016-02-01"))
                .andDo(print())
                .andExpect(flash().attribute("message", startsWith("Payment for member")))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }
}

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;

import net.bugorfeature.subman.builder.CommunityBuilder;
import net.bugorfeature.subman.loader.ImportResult;
import net.bugorfeature.subman.model.Community;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * Integration test for community controller
 *
 * http://stackoverflow.com/questions/33524509/spring-mockmvc-spring-security-and-mockito
 *
 * @author Andy Geach
 */
public class CommunityControllerITCase extends AbstractControllerITCase {

    @Test
    public void about() throws Exception {
        mockMvc.perform(get("/community/about").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("about"));
    }

    @Test
    public void contact() throws Exception {
        mockMvc.perform(get("/community/contact").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("contact"));
    }

    @Test
    public void importPage() throws Exception {
        mockMvc.perform(get("/community/import").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("import"));
    }

    @Test
    public void stats() throws Exception {
        mockMvc.perform(get("/community/stats").sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community))
                .andExpect(model().attributeExists("status", "payment", "frequency"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("stats"));
    }

    @Test
    public void listMembersForValidId() throws Exception {
        mockMvc.perform(get("/community/listMembers")
                .sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andExpect(model().attributeExists(CommunityController.COMMUNITY_OBJ_KEY))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("listMembers"));
    }

    @Test
    public void listMembersForInvalidId() throws Exception {
        mockMvc.perform(get("/community/listMembers"))
                .andExpect(model().attributeDoesNotExist(CommunityController.COMMUNITY_ID_KEY))
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/list"));
    }

    @Test
    public void viewPage() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        mockMvc.perform(get("/community/{id}", communityId)
                .sessionAttr(CommunityController.COMMUNITY_ID_KEY, communityId))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(view().name("community"));
    }

    @Test
    public void select() throws Exception {
        when(communityService.getCommunityForId(anyInt())).thenReturn(community);
        mockMvc.perform(get("/community/{id}/select", communityId))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void listNone() throws Exception {
        List<Community> list = new ArrayList<>();
        when(communityService.getCommunitiesForUsername(anyString())).thenReturn(list);
        mockMvc.perform(get("/community/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("error"));
    }


    @Test
    @WithMockUser(username = "user@example.org", roles = "USER")
    public void listOne() throws Exception {
        List<Community> list = new ArrayList<>();
        list.add(CommunityBuilder.aCommunity().build());
        when(communityService.getCommunitiesForUsername(anyString())).thenReturn(list);
        mockMvc.perform(get("/community/list"))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/community/listMembers"));
    }

    @Test
    public void listTwo() throws Exception {
        List<Community> list = new ArrayList<>();
        list.add(CommunityBuilder.aCommunity().name("one").build());
        list.add(CommunityBuilder.aCommunity().name("two").build());
        when(communityService.getCommunitiesForUsername(anyString())).thenReturn(list);
        mockMvc.perform(get("/community/list"))
                .andExpect(status().isOk())
                .andExpect(view().name("communityList"));
    }

    @Test
    @WithMockUser(username = "user@example.org", roles = "USER")
    public void importFileSuccess() throws Exception {
        when(importService.doImport(any(Community.class), any(InputStream.class))).thenReturn(new ImportResult());
        MockMultipartFile file = new MockMultipartFile("file", "import.csv", "text/csv", "some csv".getBytes());
        mockMvc.perform(fileUpload("/community/doImport").file(file).sessionAttr(CommunityController.COMMUNITY_OBJ_KEY, community).with(csrf()))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("importResult"))
                .andExpect(view().name("importResult"));
    }
}

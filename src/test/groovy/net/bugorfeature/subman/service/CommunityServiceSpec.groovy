/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service

import net.bugorfeature.subman.model.Community
import net.bugorfeature.subman.repo.CommunityRepository
import spock.lang.Specification

/**
 * Specification for CommunityService
 *
 * @author Andy Geach
 */
class CommunityServiceSpec extends Specification {

    CommunityServiceHandler service
    CommunityRepository mockRepo
    def result

    def setup() {

        service = new CommunityServiceHandler()
        mockRepo = Mock(CommunityRepository)
        service.setCommunityRepository(mockRepo)
    }

    void 'should return Community instance given ID'() {

        given:
           1 * mockRepo.findOne(_) >> new Community()

        when:
            result = service.getCommunityForId(1)

        then:
            service.getCommunityRepository() != null
            assert result != null
            assert result instanceof Community
    }

    void 'should return collection of Community instances given username'() {

        given:
            1 * mockRepo.findByUsersUsername(_) >> [new Community(), new Community()]

        when:
            result = service.getCommunitiesForUsername("foo")

        then:
            service.getCommunityRepository() != null
            assert result != null
            assert result.size() == 2
    }
}

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.service

import net.bugorfeature.subman.model.Member
import net.bugorfeature.subman.repo.MemberRepository
import spock.lang.Specification

/**
 * Specification for MemberService
 *
 * @author Andy Geach
 */
class MemberServiceSpec extends Specification {

    MemberServiceHandler service
    MemberRepository mockRepo
    def result

    def setup() {

        service = new MemberServiceHandler()
        mockRepo = Mock(MemberRepository)
        service.setMemberRepository(mockRepo)
    }

    void 'should return Member instance given member and community ids'() {

        given:
           1 * mockRepo.findByIdNumberAndSubscriptionsCommunityIdNumber(_, _) >> new Member()

        when:
            result = service.getMember(1, 1)

        then:
            service.getMemberRepository() != null
            assert result != null
            assert result instanceof Member
    }

    void 'should return Member instance on save'() {

        given:
            Member member = new Member()
            1 * mockRepo.save(_) >> member

        when:
            result = service.save(member)

        then:
            service.getMemberRepository() != null
            assert result != null
            assert result instanceof Member
    }
}

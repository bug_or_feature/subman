/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv

import net.bugorfeature.subman.model.SalutationType
import org.supercsv.cellprocessor.CellProcessorAdaptor

/**
 * Specification for SalutationTypeProcessor
 *
 * @author Andy Geach
 */
class SalutationTypeProcessorSpec extends CsvProcessorSpec {

    SalutationTypeProcessor processor

    def setup() {
        processor = new SalutationTypeProcessor()
    }

    void 'process SalutationType values correctly'() {
        expect:
            processor.execute("Mr", context) == SalutationType.MR
            processor.execute("Mr Andrews", context) == SalutationType.MR
            processor.execute("Mrs", context) == SalutationType.MRS
            processor.execute("Miss", context) == SalutationType.MISS
            processor.execute("Dr", context) == SalutationType.DR
            processor.execute("Dr Livingstone", context) == SalutationType.DR
            processor.execute("Lady", context) == SalutationType.LADY
            processor.execute("Colonel", context) == SalutationType.COL
    }

    @Override
    CellProcessorAdaptor processor() {
        return processor
    }
}

/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv

import net.bugorfeature.subman.model.SubscriptionFrequency
import org.supercsv.cellprocessor.CellProcessorAdaptor

/**
 * Specification for FrequencyProcessor
 *
 * @author Andy Geach
 */
class FrequencyProcessorSpec extends CsvProcessorSpec {

    FrequencyProcessor processor

    def setup() {
        processor = new FrequencyProcessor()
    }

    void 'process Frequency values correctly'() {
        expect:
            processor.execute("Annually", context) == SubscriptionFrequency.ANNUAL
            processor.execute("Bi-annually", context) == SubscriptionFrequency.BI_ANNUAL
            processor.execute("Monthly", context) == SubscriptionFrequency.MONTH
            processor.execute("Quarterly", context) == SubscriptionFrequency.QUARTER
    }

    @Override
    CellProcessorAdaptor processor() {
        return processor
    }
}

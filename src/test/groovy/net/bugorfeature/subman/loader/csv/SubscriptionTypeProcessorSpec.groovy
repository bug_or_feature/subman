/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv

import net.bugorfeature.subman.model.SubscriptionType
import org.supercsv.cellprocessor.CellProcessorAdaptor

/**
 * Specification for SubscriptionTypeProcessor
 *
 * @author Andy Geach
 */
class SubscriptionTypeProcessorSpec extends CsvProcessorSpec {

    SubscriptionTypeProcessor processor

    def setup() {
        processor = new SubscriptionTypeProcessor()
    }

    void 'process SubscriptionType values correctly'() {
        expect:
            processor.execute("Member", context) == SubscriptionType.MEMBER
            processor.execute("Friend", context) == SubscriptionType.FRIEND
            processor.execute("Youth", context) == SubscriptionType.YOUTH
            processor.execute("Armed Forces", context) == SubscriptionType.ARMED_FORCES
            processor.execute("CAMS", context) == SubscriptionType.CAMS
    }

    @Override
    CellProcessorAdaptor processor() {
        return processor
    }
}

INSERT INTO community(COMMUNITY_ID, NAME, ABOUT) VALUES (1, 'Bury St Edmunds Tango Club', 'Tango Club based in Bury St Edmunds in Suffolk');

INSERT INTO community(COMMUNITY_ID, NAME, ABOUT) VALUES (2, 'Herne Hill Cycling Club', 'Cycle club based at the world famous Velodrome in Herne Hill, South East London. Many members specialise in wearing spandex clothing and silly hats');

INSERT INTO member(MEMBER_ID, FIRST_NAME, INITIALS, LAST_NAME, GENDER, ADDRESS_1, ADDRESS_2, ADDRESS_3, TOWN_CITY, COUNTY, POSTCODE, TELEPHONE_NO, EMAIL_ADDRESS, SALUTATION, COMM_PREFS) VALUES
  (1, 'Andrew', 'A', 'Anderson','M', '1 Alpaca St', 'Aldrington','', 'Bury St Edmunds', 'Suffolk','IP33 1AA', '01284 000 001', 'aaa@bsetc.org', 'MR', 'NONE'),
  (2, 'Bart', 'BB', 'Barry','M', '2 Boris Walk', '','', 'Bury St Edmunds', 'Suffolk','IP33 2BB', '01284 000 002', 'bbb@bsetc.org', 'MR', 'NONE'),
  (3, 'Charlie', '', 'Chester','M', '3 Carter Road', '','', 'Bury St Edmunds', 'Suffolk','IP33 3CC', '01284 000 003', 'ccc@bsetc.org', 'MR', 'NONE'),
  (4, 'David', 'D', 'Doolittle','M', '4 Dimple Gardens', 'Despage','', 'Bury St Edmunds', 'Suffolk','IP33 4DD', '01284 000 004', 'ddd@bsetc.org', 'MR', 'NONE'),
  (5, 'Eleanor', 'E', 'Epping','F', '5 Exeter Place', '','', 'Bury St Edmunds', 'Suffolk','IP33 5EE', '01284 000 005', '', 'MS', 'NONE'),
  (6, 'Francis', 'FF', 'Fogerty','F', '6 Ford Way', '','', 'Bury St Edmunds', 'Suffolk','IP33 6FF', '01284 000 006', 'fff@bsetc.org', 'MRS', 'NONE'),
  (7, 'Gemma', 'G', 'Grant','F', 'Gazza Cottage', '7 Gerry Vale', 'Gimp', 'Bury St Edmunds', 'Suffolk','IP33 7GG', '01284 000 007', 'ggg@bsetc.org', 'MISS', 'NONE'),
  (8, 'Harry', '', 'Handlebar','M', '8 Holmes Place', '','', 'Bury St Edmunds', 'Suffolk','IP33 8HH', '01284 000 008', 'hhh@bsetc.org', 'MR', 'NONE'),
  (9, 'Ian', 'I', 'Igloo','M', '9 Innaloo Close', '','', 'Bury St Edmunds', 'Suffolk','IP31 9II', '01284 000 009', 'iii@bsetc.org', 'MR', 'NONE'),
  (10, 'Albert', 'A', 'Andrews','M', '1 Allan St', '','', 'London', 'Greater London','SE24 1AA', '020 0000 0001', 'aaa@hhcc.org', 'MR', 'NONE'),
  (11, 'Barry', 'BB', 'Berthold','M', '2 Birch Walk', '','', 'London', 'Greater London','SE24 2BB', '020 0000 0002', 'bbb@hhcc.org', 'MR', 'NONE'),
  (12, 'Chris', '', 'Cringle','M', '3 Cuthbert Road', '','', 'London', 'Greater London','SE24 3CC', '020 0000 0003', 'ccc@hhcc.org', 'MR', 'NONE'),
  (13, 'Donald', 'D', 'Davidson','M', '4 Dingle Gardens', '','', 'London', 'Greater London','SE24 4DD', '020 0000 0004', 'ddd@hhcc.org', 'MR', 'NONE'),
  (14, 'Ellen', 'E', 'Elderson','F', '5 Earlsfield Place', '','', 'London', 'Greater London','SE24 5EE', '020 0000 0005', '', 'MS', 'NONE'),
  (15, 'Fred', 'FF', 'Farthington','F', '6 Frank Way', '','', 'London', 'Greater London','SE24 6FF', '020 0000 0006', 'fff@hhcc.org', 'MRS', 'NONE'),
  (16, 'Gillian', 'G', 'Gilbert','F', 'Gough Cottage', '7 Greatorex Way', '', 'London', 'Greater London','SE24 7GG', '020 0000 0007', 'ggg@hhcc.org', 'MISS', 'NONE'),
  (17, 'Hugh', '', 'Hewetson','M', '8 Hiscott Place', '','', 'London', 'Greater London','SE24 8HH', '020 0000 0008', 'hhh@hhcc.org', 'MR', 'NONE'),
  (18, 'Iain', 'I', 'Icehouse','M', '9 Iverson Close', '','', 'London', 'Greater London','SE24 9II', '020 0000 0009', 'iii@hhcc.org', 'MR', 'NONE');

INSERT INTO subscription (COMMUNITY_ID, MEMBER_ID, SUB_NUMBER, DUE_AMOUNT, DUE_DATE, PAID_AMOUNT, PAID_DATE, PAYMENT_METHOD, FREQUENCY, STATUS) VALUES
  (1, 1,  'BSETC-1', 25.00, '2016-05-01', 25.00, '2016-05-01', 'BANK_TRANSFER', 'ANNUAL', 'EXPIRED'),
  (1, 2,  'BSETC-2', 50.00, '2016-05-01', 50.00, '2016-05-01', 'STANDING_ORDER', 'ANNUAL', 'CURRENT'),
  (1, 3,  'BSETC-3', 35.00, '2016-05-01', 35.00, '2016-05-01', 'CHEQUE', 'QUARTER', 'CURRENT'),
  (1, 4,  'BSETC-4', 25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'BI_ANNUAL', 'CURRENT'),
  (1, 5,  'BSETC-5', 10.00, '2016-05-01', 10.00, '2016-05-01', 'CREDIT_CARD', 'ANNUAL', 'CURRENT'),
  (1, 6,  'BSETC-6', 25.00, '2016-05-01', 25.00, '2016-05-01', 'DIRECT_DEBIT', 'ANNUAL', 'CURRENT'),
  (1, 7,  'BSETC-7', 40.00, '2016-05-01', 40.00, '2016-05-01', 'CHEQUE', 'ANNUAL', 'CURRENT'),
  (1, 8,  'BSETC-8', 25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'QUARTER', 'CURRENT'),
  (1, 9,  'BSETC-9', 25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'QUARTER', 'CURRENT'),
  (2, 10, 'HHCC-1',  25.00, '2016-05-01', 25.00, '2016-05-01', 'BANK_TRANSFER', 'ANNUAL', 'EXPIRED'),
  (2, 11, 'HHCC-2',  50.00, '2016-05-01', 50.00, '2016-05-01', 'STANDING_ORDER', 'ANNUAL', 'CURRENT'),
  (2, 12, 'HHCC-3',  35.00, '2016-05-01', 35.00, '2016-05-01', 'CHEQUE', 'QUARTER', 'CURRENT'),
  (2, 13, 'HHCC-4',  25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'BI_ANNUAL', 'CURRENT'),
  (2, 14, 'HHCC-5',  10.00, '2016-05-01', 10.00, '2016-05-01', 'CREDIT_CARD', 'ANNUAL', 'CURRENT'),
  (2, 15, 'HHCC-6',  25.00, '2016-05-01', 25.00, '2016-05-01', 'DIRECT_DEBIT', 'ANNUAL', 'CURRENT'),
  (2, 16, 'HHCC-7',  40.00, '2016-05-01', 40.00, '2016-05-01', 'CHEQUE', 'ANNUAL', 'CURRENT'),
  (2, 17, 'HHCC-8',  25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'QUARTER', 'CURRENT'),
  (2, 18, 'HHCC-9',  25.00, '2016-05-01', 25.00, '2016-05-01', 'STANDING_ORDER', 'QUARTER', 'CURRENT');

INSERT INTO user_table (USERNAME, PASSWORD, ENABLED) VALUES
  ('user@example.org', '$2a$10$9DPJVvPqce4RbESwSdv7te6uxwKo54lJUQonNj75Cjd3IeHYOdliK', TRUE);

INSERT INTO authority (USERNAME, AUTHORITY) VALUES
  ('user@example.org', 'USER'),
  ('user@example.org', 'ADMIN');

INSERT INTO user_community (COMMUNITY_ID, USERNAME) VALUES
  (1, 'user@example.org'),
  (2, 'user@example.org');

INSERT INTO status_rule (STATUS_RULE_ID, COMMUNITY_ID, DAY_COUNT, STATUS) VALUES
  (1, 1, 5, 'EXPIRED'),
  (2, 2, 5, 'EXPIRED');

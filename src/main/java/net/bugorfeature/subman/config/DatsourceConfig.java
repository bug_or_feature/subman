/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.config;

import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Custom flyway migration strategy that cleans db tables before migrations for integration tests
 *
 * @author Andy Geach
 */
@Configuration
@Profile("itcase")
public class DatsourceConfig {

    @Bean
    public CleaningFlywayMigrationStrategy getFlywayCleaningStrategy() {
        return new CleaningFlywayMigrationStrategy();
    }


    private static class CleaningFlywayMigrationStrategy implements FlywayMigrationStrategy {

        /**
         * Trigger flyway migration.
         *
         * @param flyway the flyway instance
         */
        @Override
        public void migrate(Flyway flyway) {
            flyway.clean();
            flyway.migrate();
        }
    }
}

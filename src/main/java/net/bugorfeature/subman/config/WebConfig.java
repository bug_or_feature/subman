/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.config;

import javax.servlet.MultipartConfigElement;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafProperties;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.Resource;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.XmlViewResolver;
import org.thymeleaf.dialect.IDialect;
import org.thymeleaf.spring4.SpringTemplateEngine;
import org.thymeleaf.spring4.resourceresolver.SpringResourceResourceResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.thymeleaf.templateresolver.TemplateResolver;

import net.bugorfeature.subman.view.ThymeleafCustomTemplateResolver;

/**
 * Web security config
 *
 * @author Andy Geach
 */
@Configuration
@EnableWebSecurity
@Import(PasswordEncoderConfig.class)
public class WebConfig extends WebMvcConfigurerAdapter {

    @Value("classpath:views.xml")
    private Resource viewConfig;

    @Autowired
    private ThymeleafProperties thymeleafProperties;

    @Autowired(required = false)
    private final Collection<IDialect> dialects = Collections.emptySet();

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/login").setViewName("login");
        registry.addViewController("/contact").setViewName("contact");
        registry.addViewController("/error").setViewName("error");
    }

    @Bean
    public MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("1024KB");
        factory.setMaxRequestSize("1024KB");
        return factory.createMultipartConfig();
    }

    @Bean
    public XmlViewResolver xmlViewResolver() {
        XmlViewResolver resolver = new XmlViewResolver();
        resolver.setOrder(0);
        resolver.setLocation(viewConfig);
        return resolver;
    }

    @Bean
    @Order(Ordered.HIGHEST_PRECEDENCE + 10)
    public AuthenticationConfig authenticationSecurity() {
        return new AuthenticationConfig();
    }

    @Bean
    public ITemplateResolver submanTemplateResolver() {
        ThymeleafCustomTemplateResolver resolver = new ThymeleafCustomTemplateResolver();
        resolver.setResourceResolver(thymeleafResourceResolver());
        resolver.setPrefix(this.thymeleafProperties.getPrefix());
        resolver.setSuffix(this.thymeleafProperties.getSuffix());
        resolver.setTemplateMode(this.thymeleafProperties.getMode());
        resolver.setCharacterEncoding(this.thymeleafProperties.getEncoding().name());
        resolver.setCacheable(this.thymeleafProperties.isCache());
        resolver.setSessionVariableName("communityName");
        resolver.setOrder(1);
        return resolver;
    }

    @Bean
    public ITemplateResolver defaultTemplateResolver() {
        TemplateResolver resolver = new TemplateResolver();
        resolver.setResourceResolver(thymeleafResourceResolver());
        resolver.setPrefix(this.thymeleafProperties.getPrefix());
        resolver.setSuffix(this.thymeleafProperties.getSuffix());
        resolver.setTemplateMode(this.thymeleafProperties.getMode());
        resolver.setCharacterEncoding(this.thymeleafProperties.getEncoding().name());
        resolver.setCacheable(this.thymeleafProperties.isCache());
        resolver.setOrder(2);
        return resolver;
    }

    @Bean
    public SpringTemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.addTemplateResolver(defaultTemplateResolver());
        engine.addTemplateResolver(submanTemplateResolver());
        for (IDialect dialect : this.dialects) {
            engine.addDialect(dialect);
        }
        return engine;
    }

    @Bean
    public SpringResourceResourceResolver thymeleafResourceResolver() {
        return new SpringResourceResourceResolver();
    }
}

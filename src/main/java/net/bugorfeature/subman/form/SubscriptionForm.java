/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.form;

import java.math.BigDecimal;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.springframework.format.annotation.DateTimeFormat;

import org.joda.time.LocalDate;

import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.SubscriptionType;

/**
 * Form backing object for Subscription instances
 *
 * @author Andy Geach
 */
public class SubscriptionForm {

    Integer idNumber;

    //@NotBlank
    private String number;                        // non editable

    @DecimalMin("0")
    private BigDecimal dueAmount;                 // set once

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;                         // auto set

    @DecimalMin("0")
    private BigDecimal paidAmount;                // edit

    @Past
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate paidDate;                         // edit

    @NotNull
    private SubscriptionType subscriptionType;      // set once

    @NotNull
    private SubscriptionFrequency frequency;        // set once

    @NotNull
    private SubscriptionStatus status;              // non editable

    @NotNull
    private PaymentMethod paymentMethod;            // set once

    private Boolean cardIssued;

    private String customField_1;                   // set once

    private String firstName;

    private String lastName;

    private Integer memberId;

    private String notes;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate memberSince;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate originalDueDate;                         // auto set

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(BigDecimal dueAmount) {
        this.dueAmount = dueAmount;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public LocalDate getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(LocalDate paidDate) {
        this.paidDate = paidDate;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public SubscriptionFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(SubscriptionFrequency frequency) {
        this.frequency = frequency;
    }

    public SubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(SubscriptionStatus status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Boolean getCardIssued() {
        return cardIssued;
    }

    public void setCardIssued(Boolean cardIssued) {
        this.cardIssued = cardIssued;
    }

    public String getCustomField_1() {
        return customField_1;
    }

    public void setCustomField_1(String customField_1) {
        this.customField_1 = customField_1;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public LocalDate getOriginalDueDate() {
        return originalDueDate;
    }

    public void setOriginalDueDate(LocalDate originalDueDate) {
        this.originalDueDate = originalDueDate;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDate memberSince) {
        this.memberSince = memberSince;
    }
}

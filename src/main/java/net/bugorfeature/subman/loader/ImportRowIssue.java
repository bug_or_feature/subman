/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Represents an issue with the import of one row of CSV data
 *
 * @author Andy Geach
 */
public class ImportRowIssue {

    private int rowNum;

    private int colNum;

    private String colName;

    private String message;

    public ImportRowIssue(int rowNum, String message) {
        this(rowNum, -1, "unknown", message);
    }

    public ImportRowIssue(int rowNum, int colNum, String colName, String message) {
        this.rowNum = rowNum;
        this.colNum = colNum;
        this.colName = colName;
        this.message = message;
    }

    public int getRowNum() {
        return rowNum;
    }

    public int getColNum() {
        return colNum;
    }

    public String getMessage() {
        return message;
    }

    public String getColName() {
        return colName;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).
                append("rowNum", rowNum).
                append("colNum", colNum).
                append("colName", colName).
                append("message", message).toString();
    }
}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Represents the results of an attempt to import records
 *
 * @author Andy Geach
 */
public class ImportResult {

    private List<ImportRowIssue> issues = new ArrayList<>();

    private int successCount;

    public void incrementSuccess() {
        successCount++;
    }

    public void addIssue(int rowNum, int colNum, String colName, String message) {
        issues.add(new ImportRowIssue(rowNum, colNum, colName, message));
    }

    public void addIssue(int rowNum, String message) {
        issues.add(new ImportRowIssue(rowNum, message));
    }

    public Collection<ImportRowIssue> getIssues() {
        return issues;
    }

    public ImportRowIssue get(int index) {
        return issues.get(index);
    }

    public void setIssues(List<ImportRowIssue> issues) {
        this.issues = issues;
    }

    public int getSuccessCount() {
        return successCount;
    }

    public void setSuccessCount(int successCount) {
        this.successCount = successCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("successCount", successCount)
                .toString();
    }
}

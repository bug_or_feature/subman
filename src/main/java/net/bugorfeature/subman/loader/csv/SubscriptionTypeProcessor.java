/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader.csv;

import org.supercsv.cellprocessor.CellProcessorAdaptor;
import org.supercsv.cellprocessor.ift.StringCellProcessor;
import org.supercsv.exception.SuperCsvCellProcessorException;
import org.supercsv.util.CsvContext;

import net.bugorfeature.subman.model.SubscriptionType;

/**
 * Csv cell processor for SubscriptionType instances
 *
 * @author Andy Geach
 */
public class SubscriptionTypeProcessor extends CellProcessorAdaptor implements StringCellProcessor {
    @Override
    public Object execute(Object value, CsvContext context) {
        validateInputNotNull(value, context);

        final SubscriptionType result;
        if (value instanceof String) {
            final String s = (String) value;
            try {
                result = SubscriptionType.fromString(s);
            } catch (final Exception e) {
                throw new SuperCsvCellProcessorException(String.format("'%s' could not be parsed as a SubscriptionType instance.",
                        value), context, this, e);
            }
        } else {
            throw new SuperCsvCellProcessorException(String.class, value, context, this);
        }

        return next.execute(result, context);
    }
}

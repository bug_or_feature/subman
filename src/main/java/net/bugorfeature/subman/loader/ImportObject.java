/*
 * Copyright 2016 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.loader;

import java.math.BigDecimal;

import org.joda.time.LocalDate;

import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.PaymentMethod;
import net.bugorfeature.subman.model.SalutationType;
import net.bugorfeature.subman.model.SubscriptionFrequency;
import net.bugorfeature.subman.model.SubscriptionStatus;
import net.bugorfeature.subman.model.SubscriptionType;

/**
 * Represents a row of CSV data for import files
 *
 * @author Andy Geach
 */
public class ImportObject {

    private SalutationType salutation;
    private String firstName;
    private String lastName;
    private String initials;
    private Gender gender;
    private String address1;
    private String address2;
    private String address3;
    private String townCity;
    private String county;
    private String postCode;
    private String telephoneNo;
    private String emailAddress;
    private CommunicationPreferencesType commPrefs;
    private String number;
    private BigDecimal dueAmount;
    private LocalDate dueDate;
    private BigDecimal paidAmount;
    private LocalDate paidDate;
    private SubscriptionType subscriptionType;
    private SubscriptionFrequency frequency;
    private SubscriptionStatus status;
    private PaymentMethod paymentMethod;
    private boolean cardIssued;
    private String notes;
    private LocalDate memberSince;
    private String customMemberField;
    private String customSubscriptionField;

    // non null instance, to differentiate from end of stream marker
    static ImportObject EMPTY_INSTANCE = new ImportObject();

    public SalutationType getSalutation() {
        return salutation;
    }

    public void setSalutation(SalutationType salutation) {
        this.salutation = salutation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getTownCity() {
        return townCity;
    }

    public void setTownCity(String townCity) {
        this.townCity = townCity;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getTelephoneNo() {
        return telephoneNo;
    }

    public void setTelephoneNo(String telephoneNo) {
        this.telephoneNo = telephoneNo;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public CommunicationPreferencesType getCommPrefs() {
        return commPrefs;
    }

    public void setCommPrefs(CommunicationPreferencesType commPrefs) {
        this.commPrefs = commPrefs;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public BigDecimal getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(BigDecimal dueAmount) {
        this.dueAmount = dueAmount;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public LocalDate getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(LocalDate paidDate) {
        this.paidDate = paidDate;
    }

    public SubscriptionType getSubscriptionType() {
        return subscriptionType;
    }

    public void setSubscriptionType(SubscriptionType subscriptionType) {
        this.subscriptionType = subscriptionType;
    }

    public SubscriptionFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(SubscriptionFrequency frequency) {
        this.frequency = frequency;
    }

    public SubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(SubscriptionStatus status) {
        this.status = status;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public boolean isCardIssued() {
        return cardIssued;
    }

    public void setCardIssued(boolean cardIssued) {
        this.cardIssued = cardIssued;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDate memberSince) {
        this.memberSince = memberSince;
    }

    public String getCustomMemberField() {
        return customMemberField;
    }

    public void setCustomMemberField(String customMemberField) {
        this.customMemberField = customMemberField;
    }

    public String getCustomSubscriptionField() {
        return customSubscriptionField;
    }

    public void setCustomSubscriptionField(String customSubscriptionField) {
        this.customSubscriptionField = customSubscriptionField;
    }
}

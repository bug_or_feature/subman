/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.repo;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Subscription;

/**
 * Repository for Subscription instances
 *
 * @author Andy Geach
 */
@Repository
public interface SubscriptionRepository extends CrudRepository<Subscription, Integer> {

    Collection<Subscription> findByCommunity(Community community);

    Subscription findByMemberIdNumber(Integer memberId);

    Subscription findByMemberIdNumberAndCommunityIdNumber(Integer memberId, Integer communityId);

    Subscription findByNumber(String number);

    @Query(value = "select status, count(*) from subscription where community = :community group by status order by status")
    List<Object[]> getStatusStats(@Param("community") Community community);

    @Query(value = "select customField_1, count(*) from subscription where community = :community " +
            "group by customField_1 order by customField_1")
    List<Object[]> getBranchStats(@Param("community") Community community);

    @Query(value = "select frequency, count(*) from subscription where community = :community " +
            "group by frequency order by frequency")
    List<Object[]> getFrequencyStats(@Param("community") Community community);

    @Query(value = "select paymentMethod, count(*) from subscription where community = :community " +
            "group by paymentMethod order by paymentMethod")
    List<Object[]> getPaymentTypeStats(@Param("community") Community community);

    @Query(value = "select m.customField_1, count(*) from subscription s join s.member m where s.community = :community " +
            "group by m.customField_1 order by m.customField_1")
    List<Object[]> getWardStats(@Param("community") Community community);
}

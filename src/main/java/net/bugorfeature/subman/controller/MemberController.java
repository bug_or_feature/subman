/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.bugorfeature.subman.form.MemberForm;
import net.bugorfeature.subman.model.CommunicationPreferencesType;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.model.Gender;
import net.bugorfeature.subman.model.Member;
import net.bugorfeature.subman.model.SalutationType;
import net.bugorfeature.subman.model.Ward;
import net.bugorfeature.subman.service.MemberService;

/**
 * Member endpoint controller
 *
 * @author Andy Geach
 */
@Controller
@RequestMapping("/member")
@SessionAttributes({CommunityController.COMMUNITY_OBJ_KEY, "salutations", "genders", "wards", "commPrefsTypes"})
public class MemberController {

    private static final Logger log = LoggerFactory.getLogger(MemberController.class);
    public static final String CREATE_OR_EDIT_MEMBER = "createOrEditMember";

    @Autowired
    private MemberService service;

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String setupNew(Model model) {

        model.addAttribute("salutations", SalutationType.values()); // TODO rename
        model.addAttribute("genders", Gender.values()); // TODO rename
        model.addAttribute("wards", Ward.values()); // TODO remove
        model.addAttribute("commPrefsTypes", CommunicationPreferencesType.values()); // TODO rename
        return CREATE_OR_EDIT_MEMBER;
    }

    @ModelAttribute("member")
    public MemberForm addMember() {
        return new MemberForm();
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String setupEdit(HttpSession session, @PathVariable("id") Integer memberId, Model model, RedirectAttributes redirect) {

        Community current = (Community)session.getAttribute(CommunityController.COMMUNITY_OBJ_KEY);
        Member member = service.getMember(memberId, current.getIdNumber());

        if (member == null) {
            redirect.addFlashAttribute("warning", String.format("No member found with ID %d", memberId));
            return "redirect:/community/listMembers";
        }

        MemberForm memberForm = convert(member);

        model.addAttribute("member", memberForm);
        model.addAttribute("salutations", SalutationType.values()); // TODO rename
        model.addAttribute("genders", Gender.values()); // TODO rename
        model.addAttribute("wards", Ward.values()); // TODO remove
        model.addAttribute("commPrefsTypes", CommunicationPreferencesType.values()); // TODO rename
        return CREATE_OR_EDIT_MEMBER;
    }

    /**
     *
     * TODO postcodeanywhere.co.uk
     *
     * @param form
     * @param result
     * @param status
     * @param redirect
     * @return
     */
    @RequestMapping(value = "/submit", method = RequestMethod.POST)
    public String processUpdate(HttpSession session, @Valid @ModelAttribute("member") MemberForm form,
            BindingResult result, SessionStatus status, RedirectAttributes redirect) {

        if (result.hasErrors()) {
            return CREATE_OR_EDIT_MEMBER;
        }

        if (form.getIdNumber() != null) {
            Community current = (Community)session.getAttribute(CommunityController.COMMUNITY_OBJ_KEY);
            Member member = convert(form, current);

            service.save(member);

            String message = String.format("Member '%s %s' saved successfully", member.getFirstName(), member.getLastName());
            status.setComplete();

            redirect.addFlashAttribute("message", message);
            return "redirect:/community/listMembers";

        } else {
            session.setAttribute("newMember", form);
            return "redirect:/subscription/new";
        }
    }

    private Member convert(MemberForm form, Community current) {
        Member member = service.getMember(form.getIdNumber(), current.getIdNumber());
        try {
            BeanUtils.copyProperties(member, form);
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        return member;
    }

    private MemberForm convert(Member member) {
        MemberForm form = new MemberForm();
        try {
            BeanUtils.copyProperties(form, member);
        } catch (Exception e) {
            log.error("Problem: " + e);
        }

        return form;
    }
}

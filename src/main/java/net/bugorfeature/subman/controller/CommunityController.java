/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.controller;

import net.bugorfeature.subman.loader.ImportResult;
import net.bugorfeature.subman.model.Community;
import net.bugorfeature.subman.service.CommunityService;
import net.bugorfeature.subman.service.ImportService;
import net.bugorfeature.subman.service.SubscriptionService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Community endpoint controller
 *
 * @author Andy Geach
 */
@Controller
@RequestMapping("/community")
@SessionAttributes({CommunityController.COMMUNITY_OBJ_KEY})
public class CommunityController {

    private static final Logger log = LoggerFactory.getLogger(CommunityController.class);

    public static final String COMMUNITY_ID_KEY = "currentCommunity";
    public static final String COMMUNITY_OBJ_KEY = "communityObj";

    @Autowired
    private CommunityService communityService;

    @Autowired
    private SubscriptionService subscriptionService;

    @Autowired
    private ImportService importService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(HttpSession session, @AuthenticationPrincipal UserDetails currentUser, Model model) {
        List<Community> communities = communityService.getCommunitiesForUsername(currentUser.getUsername());
        session.setAttribute("canSwitch", communities.size() != 1);
        if (communities.isEmpty()) {
            return "error"; // TODO ??

        } else if (communities.size() == 1) {
            Community current = communities.get(0);
            session.setAttribute(COMMUNITY_ID_KEY, current.getIdNumber());
            session.setAttribute("communityName", current.getName());
            session.setAttribute(COMMUNITY_OBJ_KEY, current); // TODO ??
            model.addAttribute(COMMUNITY_OBJ_KEY, current);
            return "redirect:/community/listMembers";

        } else {
            model.addAttribute("communityList", communities);
            return "communityList";
        }
    }

    /**
     * @param session
     * @param communityId
     * @param model
     * @return
     */
    @RequestMapping(value = "/{communityId}", method = RequestMethod.GET)
    public String view(HttpSession session, @PathVariable("communityId") Integer communityId, Model model) {
        session.setAttribute(COMMUNITY_ID_KEY, communityId);
        model.addAttribute(COMMUNITY_OBJ_KEY, communityService.getCommunityForId(communityId));
        return "community";
    }

    @RequestMapping(value = "/{communityId}/select", method = RequestMethod.GET)
    public String select(HttpSession session, @PathVariable("communityId") Integer communityId, Model model) {
        Community current = communityService.getCommunityForId(communityId);
        session.setAttribute(COMMUNITY_ID_KEY, communityId);
        session.setAttribute("communityName", current.getName());
        session.setAttribute(COMMUNITY_OBJ_KEY, current); // TODO ??
        model.addAttribute(COMMUNITY_OBJ_KEY, current);
        return "redirect:/community/listMembers";
    }

    @RequestMapping(value = "/listMembers", method = RequestMethod.GET)
    public String listBetter(HttpSession session, Model model) {
        Integer currentSelected = (Integer)session.getAttribute(COMMUNITY_ID_KEY);
        if (currentSelected == null) {
            return "redirect:/community/list";
        }
        Community current = communityService.getCommunityForId(currentSelected);
        model.addAttribute(COMMUNITY_OBJ_KEY, current);
        return "listMembers";
    }

    @RequestMapping(value = "/about", method = RequestMethod.GET)
    public String about() {
        return "about";
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public String contact() {
        return "contact";
    }

    @RequestMapping(value = "/import", method = RequestMethod.GET)
    public String showImport() {
        return "import";
    }

    @RequestMapping(value = "/doImport", method = RequestMethod.POST)
    public String doImport(@RequestParam("file") MultipartFile file, @ModelAttribute(COMMUNITY_OBJ_KEY) Community community, Model model) throws IOException {

        log.info("Attempting import from file with name=" + file.getName() + ", content type=" + file.getContentType());

        ImportResult result = importService.doImport(community, file.getInputStream());
        model.addAttribute("importResult", result);

        return "importResult";
    }

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public String status(HttpSession session, Model model) {
        Community current = (Community)session.getAttribute(COMMUNITY_OBJ_KEY);
        model.addAttribute("status", subscriptionService.getStatusStats(current));
        model.addAttribute("payment", subscriptionService.getPaymentTypeStats(current));
        model.addAttribute("frequency", subscriptionService.getFrequencyStats(current));
        return "stats";
    }
}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

/**
 * Represents possible Branches
 *
 * TODO: refactor so each Community can define their own custom fields
 *
 * @author Andy Geach
 */
public enum Branch {

    ASSOCIATION("Association"),
    BOSMERE("Bosmere"),
    BURY_TOWN("Bury Town"),
    PAKENHAM_LIVERMERE_AND_TROSTON("Pakenham, Livermere and Troston"),
    STOWMARKET("Stowmarket"),
    THURSTON_BEYTON_AND_HESSETT("Thurston, Beyton and Hessett");

    private static final Map<String, Branch> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (Branch branch : values()) {
            stringToEnum.put(branch.description, branch);
        }
    }

    private String description;

    Branch(String description) {
        this.description = description;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

    public static Branch fromString(String description) {
        Branch result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for Branch: %s", description));
        return result;
    }
}

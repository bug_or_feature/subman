/*
 * Copyright 2015 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Holds application information
 *
 * @author Andy Geach
 */
public class ApplicationInfo {

    private Map<String, String> map;

    public String getCommitTime() {
        return map.get("commitTime");
    }

    public void setCommitTime(String commitTime) {
        map.put("commitTime", commitTime);
    }

    public ApplicationInfo() {
        map = new HashMap<>();
    }

    public String getHostName() {
        return map.get("hostName");
    }

    public void setHostName(String hostName) {
        map.put("hostName", hostName);
    }

    public String getEnvironmentName() {
        return map.get("environmentName");
    }

    public void setEnvironmentName(String environmentName) {
        map.put("environmentName", environmentName);
    }

    public String getVersionNo() {
        return map.get("versionNo");
    }

    public void setVersionNo(String versionNo) {
        map.put("versionNo", versionNo);
    }

    public String getBuildTime() {
        return map.get("buildTime");
    }

    public void setBuildTime(String buildTime) {
        map.put("buildTime", buildTime);
    }

    public String getCommitId() {
        return map.get("commitId");
    }

    public void setCommitId(String commitId) {
        map.put("commitId", commitId);
    }
}

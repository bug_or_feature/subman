/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.LocalDate;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Subscription domain object. Links Communities and Members
 *
 * @author Andy Geach
 */
@Entity(name = "subscription")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Subscription implements Serializable {

    private static final long serialVersionUID = -4331478554272906564L;

    private static DateTimeFormatter formatter = ISODateTimeFormat.date(); // TODO options

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SUBSCRIPTION_ID")
    private Integer idNumber;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "MEMBER_ID")
    @JsonManagedReference
    private Member member;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name = "COMMUNITY_ID")
    @JsonManagedReference
    private Community community;

    @Column(unique = true, nullable = true, name = "SUB_NUMBER")
    private String number;

    @Column(nullable = false, name = "DUE_AMOUNT", precision = 2)
    private BigDecimal dueAmount;

    @Column(nullable = false, name = "DUE_DATE")
    private LocalDate dueDate;

    @Column(name = "PAID_AMOUNT", precision = 2)
    private BigDecimal paidAmount;

    @Column(name = "PAID_DATE")
    private LocalDate paidDate;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private SubscriptionFrequency frequency;

    @Column(name = "CUSTOM_FIELD_1")
    private String customField_1;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, name = "PAYMENT_METHOD")
    private PaymentMethod paymentMethod;

    @Column(nullable = false, name = "CARD_ISSUED")
    private Boolean cardIssued;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private SubscriptionStatus status;

    @Column(nullable = true, name = "SUB_NOTES")
    private String notes;

    @Column(nullable = true, name = "MEMBER_SINCE")
    private LocalDate memberSince;

    @JsonGetter(value = "dueDate")
    public String getJsonDueDate() {
        return formatter.print(dueDate); // TODO options;
    }

    @JsonGetter(value = "paidDate")
    public String getJsonPaidDate() {
        return paidDate == null ? "" : formatter.print(paidDate); // TODO options;
    }

    @JsonGetter(value = "memberSince")
    public String getJsonMemberSince() {
        return memberSince == null ? "" : formatter.print(memberSince); // TODO options;
    }

    @JsonGetter(value = "customField_1")
    public String getJsonCustomField_1() {
        return customField_1 == null ? "" : Branch.valueOf(customField_1).getDescription(); // TODO options;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("idNumber", idNumber)
                .append("member", member)
                .append("community", community)
                .append("number", number)
                .append("dueAmount", dueAmount)
                .append("dueDate", dueDate)
                .append("paidAmount", paidAmount)
                .append("paidDate", paidDate)
                .append("frequency", frequency)
                .append("customField_1", customField_1)
                .append("paymentMethod", paymentMethod)
                .append("cardIssued", cardIssued)
                .append("status", status)
                .append("notes", notes)
                .append("memberSince", memberSince)
                .toString();
    }

    public Integer getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(Integer idNumber) {
        this.idNumber = idNumber;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Community getCommunity() {
        return community;
    }

    public void setCommunity(Community community) {
        this.community = community;
    }

    public BigDecimal getDueAmount() {
        return dueAmount;
    }

    public void setDueAmount(BigDecimal dueAmount) {
        this.dueAmount = dueAmount;
    }

    public BigDecimal getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public LocalDate getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(LocalDate paidDate) {
        this.paidDate = paidDate;
    }

    public SubscriptionFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(SubscriptionFrequency frequency) {
        this.frequency = frequency;
    }

    public String getCustomField_1() {
        return customField_1;
    }

    public void setCustomField_1(String customField_1) {
        this.customField_1 = customField_1;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Boolean getCardIssued() {
        return cardIssued;
    }

    public void setCardIssued(Boolean cardIssued) {
        this.cardIssued = cardIssued;
    }

    public SubscriptionStatus getStatus() {
        return status;
    }

    public void setStatus(SubscriptionStatus status) {
        this.status = status;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalDate getMemberSince() {
        return memberSince;
    }

    public void setMemberSince(LocalDate memberSince) {
        this.memberSince = memberSince;
    }
}

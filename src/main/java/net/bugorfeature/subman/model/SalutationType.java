/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.apache.commons.collections4.map.CaseInsensitiveMap;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Salutation enumeration
 *
 * @author Andy Geach
 */
public enum SalutationType {

    BRIG("Brigadier"),
    CLLR("Cllr"),
    COL("Colonel"),
    COM("Commodore"),
    DR("Dr"),
    GEN("General"),
    HON("Hon"),
    LADY("Lady"),
    MISS("Miss"),
    MR("Mr"),
    MRS("Mrs"),
    MS("Ms"),
    REV("Rev"),
    SIR("Sir");


    private static final Map<String, SalutationType> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (SalutationType sal : values()) {
            stringToEnum.put(sal.description, sal);
        }
    }

    private String description;

    SalutationType(String description) {
        this.description = description;
    }

    public static SalutationType fromString(String description) {
        SalutationType result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for Salutation: %s", description));
        return result;
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

}

/*
 * Copyright 2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.bugorfeature.subman.model;

import java.util.Map;

import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonValue;
import org.apache.commons.collections4.map.CaseInsensitiveMap;

/**
 * Represents possible membership types
 *
 * TODO: refactor so each Community can have their own custom types
 *
 * @author Andy Geach
 */
public enum SubscriptionType {

    ARMED_FORCES("Armed Forces"),
    CAMS("CAMS"),
    CEM("CEM"),
    FRIEND("Friend"),
    MEMBER("Member"),
    YOUTH("Youth");

    private String description;

    SubscriptionType(String description) {
        this.description = description;
    }

    private static final Map<String, SubscriptionType> stringToEnum = new CaseInsensitiveMap<>();

    static {
        for (SubscriptionType value : values()) {
            stringToEnum.put(value.description, value);
        }
    }

    @JsonValue
    public String getDescription() {
        return description;
    }

    public static SubscriptionType fromString(String description) {
        SubscriptionType result = stringToEnum.get(description);
        Assert.notNull(result, String.format("Invalid value for SubscriptionType: %s", description));
        return result;
    }
}

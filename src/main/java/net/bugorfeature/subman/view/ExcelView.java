package net.bugorfeature.subman.view;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import net.bugorfeature.subman.model.Subscription;

/**
 * Word Excel view
 *
 * @author Andy Geach
 */
public class ExcelView extends AbstractExcelView {

    @Override
    protected void buildExcelDocument(Map model, HSSFWorkbook workbook, HttpServletRequest request,
            HttpServletResponse response) throws Exception {

        Iterable<Subscription> subs = (Iterable<Subscription>)model.get("subs");

        HSSFSheet sheet = workbook.createSheet("Member Report");

        HSSFRow header = sheet.createRow(0);
        header.createCell(0).setCellValue("SubNumber");
        header.createCell(1).setCellValue("Salutation");
        header.createCell(2).setCellValue("First name");
        header.createCell(3).setCellValue("Initials");
        header.createCell(4).setCellValue("Last name");
        header.createCell(5).setCellValue("Gender");
        header.createCell(6).setCellValue("Address 1");
        header.createCell(7).setCellValue("Address 2");
        header.createCell(8).setCellValue("Address 3");
        header.createCell(9).setCellValue("Town/City");
        header.createCell(10).setCellValue("County");
        header.createCell(11).setCellValue("Postcode");
        header.createCell(12).setCellValue("Email");
        header.createCell(13).setCellValue("Phone");
        header.createCell(14).setCellValue("Comm Prefs");
        header.createCell(15).setCellValue("Ward");
        header.createCell(16).setCellValue("Status");

        int rowNum = 1;
        for (Subscription entry : subs) {
            HSSFRow row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(entry.getNumber());
            row.createCell(1).setCellValue(entry.getMember().getSalutation().getDescription());
            row.createCell(2).setCellValue(entry.getMember().getFirstName());
            row.createCell(3).setCellValue(entry.getMember().getInitials());
            row.createCell(4).setCellValue(entry.getMember().getLastName());

            row.createCell(5).setCellValue(entry.getMember().getGender().getDescription());
            row.createCell(6).setCellValue(entry.getMember().getAddress1());
            row.createCell(7).setCellValue(entry.getMember().getAddress2());
            row.createCell(8).setCellValue(entry.getMember().getAddress3());
            row.createCell(9).setCellValue(entry.getMember().getTownCity());
            row.createCell(10).setCellValue(entry.getMember().getCounty());
            row.createCell(11).setCellValue(entry.getMember().getPostCode());
            row.createCell(12).setCellValue(entry.getMember().getEmailAddress());
            row.createCell(13).setCellValue(entry.getMember().getTelephoneNo());
            row.createCell(14).setCellValue(entry.getMember().getCommPrefs().getDescription());
            row.createCell(15).setCellValue(entry.getMember().getCustomField_1());
            row.createCell(16).setCellValue(entry.getStatus().getDescription());
        }
    }
}
